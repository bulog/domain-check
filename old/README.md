# 域名批量查询 domain-check

![演示](https://gitee.com/bulog-cn/domain-check/raw/master/old/example.gif)

#### 介绍
批量检查域名是否注册，支持.com, .net, 等主流国际、地区域名


#### 使用说明

需要在浏览器里打开。在打开的页面，输入需要查询的域名（支持通配符: (*) a-z, 0-9; (?) a-z; ($) 0-9），勾选需要查询的域名后缀就可以进行域名注册状态的查询。


#### 联系方式

ady@bulog.cn
