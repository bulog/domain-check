# 域名批量查询 domain-check

## 介绍
批量检查域名是否注册，支持.com, .net, 等主流国际、地区域名。


## 操作说明

[演示](https://bulog.cn/domain-tool.html?utm_source=source&utm_medium=gitee&utm_campaign=readme)

[![演示](https://gitee.com/bulog-cn/domain-check/raw/master/screenshoot.gif)](https://bulog.cn/domain-tool.html?utm_source=source&utm_medium=gitee&utm_campaign=readme)

在浏览器里打开查询页面，输入需要查询的域名名称（支持通配符: (*) a-z, 0-9; (?) a-z; ($) 0-9），勾选需要查询的域名后缀就可以进行域名注册状态的查询。

## 使用

### 使用已打包文件
dist内为已打包文件，可以直接部署在网站根目录

### 手动打包

```bash
npm install   # 安装依赖
npm run dev   # 测试, 成功运行之后，访问http://localhost:3000/
npm run build # 打包
```

## 联系

ady@bulog.cn
