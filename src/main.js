import { createApp } from 'vue'
import {
    ElButton,
    ElIcon,
    ElInput,
    ElCheckbox,
    ElCheckboxGroup,
    ElTabs,
    ElTabPane,
    ElRow,
    ElCol,
    ElForm,
    ElFormItem,
    ElCard,
    ElContainer,
    ElMain
} from 'element-plus';
import 'element-plus/lib/theme-chalk/base.css';
import 'element-plus/lib/theme-chalk/display.css';
import App from './App.vue'

const app = createApp(App)

app.use(ElButton);
app.use(ElIcon);
app.use(ElInput);
app.use(ElCheckbox);
app.use(ElCheckboxGroup);
app.use(ElTabs);
app.use(ElTabPane);
app.use(ElRow);
app.use(ElCol);
app.use(ElForm);
app.use(ElFormItem);
app.use(ElCard);
app.use(ElContainer);
app.use(ElMain);

app.mount('#app')
